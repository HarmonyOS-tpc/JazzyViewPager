# JazzyViewPager

An easy to use PageSlider that adds an awesome set of custom swiping animations.
Just change your PageSlider to JazzyViewPager and you're good to go!

If you're looking at the name of the library and thinking "... so what does jazz have to do with it?"
I'd like to direct you to the following definition that I found in a dictionary:

```
jazz someone or something up:
	to make someone or something more exciting or sexy;
	to make someone or something appeal more to contemporary tastes.
```

**The aim of this library is to do exactly this: jazz up the normal, everyday `PageSlider`**

This project was inspired by CyanogenMod's Trebuchet launcher.

## Usage Instructions

1. A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the sample/ folder.

2. Animations Available
    The `JazzyViewPager`'s animations are encapsulated in `TransitionEffect`.
    ```java
    public enum TransitionEffect {
            Standard,
            Stack,
            ZoomIn,
            ZoomOut,
            RotateUp,
            RotateDown,
            Accordion
    }
    ```
3. Select your animation by calling
    ```java
    private JazzyViewPager mJazzy;
    /* ... */
    mJazzy.setTransitionEffect(TransitionEffect.Accordion);
    ```
4. Setting Fade Effect
    ```java
    mJazzy.setFadeEnabled(fadeEnable);
    ```
5. Using xml
    ```
    <com.jfeinstein.jazzyviewpager.JazzyViewPager
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        xmlns:app="http://www.w3.org/1999/XSL/Transform"
        ohos:id="$+id:jazzy_pager"
        ohos:height="match_parent"
        ohos:width="match_parent"
        app:effect="11"
        app:fadeEnabled="true"/>
    ```

6. Modifying your `PageSliderProvider`
    Due to the limitations of the `PageSlider` class (which `JazzyViewPager` is built upon) in order to get the animations to work correctly for more than 3 Views, you'll have to add the following to the `createPageInContainer` method of your `PageSliderProvider`,
     ```
        private JazzyViewPager mJazzy;
        /* ... */
        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int position) {
            mJazzy.setObjectForPosition(obj, position);
            return obj;
        }
   ```     
  Once your `Object` is registered with the `JazzyViewPager`, you're good to go!

## Installation Tutorial:
1. For using JazzyViewPager in your sample application, add below dependencies:
    ```
    dependencies {
    	implementation project(path: ':lib')
    }
    ```

2. For using JazzyViewPager in separate application, add the below dependencies and include lib.har in libs folder of "entry" module:
    ```
    dependencies {
    	implementation files('libs/lib.har')
    }
    ```
   
3. For using JazzyViewPager from a remote repository in separate application, add the below dependency in entry/build.gradle file.
    ```
    dependencies {
    	implementation 'io.openharmony.tpc.thirdlib:JazzyViewPager:1.0.0'
    }
    ```

## License

    Copyright 2013 Jeremy Feinstein
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
