/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfeinstein.jazzyviewpager.sample.slice;

import com.jfeinstein.jazzyviewpager.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;


/**
 * MenuAbilitySlice
 */
public class MenuAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_menu);
        initViews();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void initViews() {
        Button accordian = (Button) findComponentById(ResourceTable.Id_accordian);
        accordian.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MainAbilitySlice(), new Intent().setParam("effect", 1));
            }
        });

        Button stack = (Button) findComponentById(ResourceTable.Id_stack);
        stack.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MainAbilitySlice(), new Intent().setParam("effect", 2));
            }
        });

        Button rotationUp = (Button) findComponentById(ResourceTable.Id_rotationUp);
        rotationUp.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MainAbilitySlice(), new Intent().setParam("effect", 3));
            }
        });

        Button rotationDown = (Button) findComponentById(ResourceTable.Id_rotationDown);
        rotationDown.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MainAbilitySlice(), new Intent().setParam("effect", 4));
            }
        });

        Button zoomIn = (Button) findComponentById(ResourceTable.Id_zoomIn);
        zoomIn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MainAbilitySlice(), new Intent().setParam("effect", 5));
            }
        });

        Button zoomOut = (Button) findComponentById(ResourceTable.Id_zoomOut);
        zoomOut.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MainAbilitySlice(), new Intent().setParam("effect", 6));
            }
        });

        Button xml = (Button) findComponentById(ResourceTable.Id_xml);
        xml.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new XmlAbilitySlice(), new Intent());
            }
        });
    }
}
