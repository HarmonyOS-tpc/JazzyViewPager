/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfeinstein.jazzyviewpager.sample.slice;

import com.jfeinstein.jazzyviewpager.JazzyViewPager;
import com.jfeinstein.jazzyviewpager.OutlineContainer;
import com.jfeinstein.jazzyviewpager.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private JazzyViewPager mJazzy;
    private ComponentContainer root;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        root = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);
        super.setUIContent(root);
        setupJazziness(getEffect(intent.getIntParam("effect", 1)));
    }

    private JazzyViewPager.TransitionEffect getEffect(int effect) {
        switch (effect) {
            case 1:return JazzyViewPager.TransitionEffect.Accordion;
            case 2:return JazzyViewPager.TransitionEffect.Stack;
            case 3:return JazzyViewPager.TransitionEffect.RotateUp;
            case 4:return JazzyViewPager.TransitionEffect.RotateDown;
            case 5:return JazzyViewPager.TransitionEffect.ZoomIn;
            case 6:return JazzyViewPager.TransitionEffect.ZoomOut;
            default:return JazzyViewPager.TransitionEffect.Standard;
        }
    }

    private void setupJazziness(JazzyViewPager.TransitionEffect effect) {
        mJazzy = (JazzyViewPager) root.findComponentById(ResourceTable.Id_jazzy_pager);
        mJazzy.setTransitionEffect(effect);
        mJazzy.setProvider(new MainAdapter());
        mJazzy.setPageMargin(30);
    }

    private class MainAdapter extends PageSliderProvider {
        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int position) {
            Text text = new Text(getContext());
            text.setTextAlignment(TextAlignment.CENTER);
            text.setTextSize(60);
            text.setTextColor(Color.BLACK);
            text.setText("Page " + position);
            text.setPadding(30, 30, 30, 30);
            int bg = Color.rgb((int) Math.floor(Math.random() * 128) + 64,
                    (int) Math.floor(Math.random() *  128) + 64,
                    (int) Math.floor(Math.random() * 128) + 64);
            ShapeElement drawable = new ShapeElement();
            drawable.setShape(ShapeElement.RECTANGLE);
            drawable.setRgbColor(RgbColor.fromArgbInt(bg));
            text.setBackground(drawable);
            componentContainer.addComponent(text, ComponentContainer.LayoutConfig.MATCH_PARENT,
                    ComponentContainer.LayoutConfig.MATCH_PARENT);
            mJazzy.setObjectForPosition(text, position);
            return text;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object obj) {
            componentContainer.removeComponent(mJazzy.findViewFromObject(position));
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object obj) {
            if (component instanceof OutlineContainer) {
                return ((OutlineContainer) component).getComponentAt(0) == obj;
            } else {
                return component == obj;
            }
        }
    }
}
